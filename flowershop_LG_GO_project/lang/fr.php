<?php


return array(
    
    // --------------------------------- About
    'about'	=> 'À propos de nous',
    'story'	=> 'En histoire de la fleur des tiges',
    'business'	=> 'Meilleur dans entreprise',
    'stemsFlower'	=> 'Stems Flower est une boutique en ligne de renom qui propose depuis le premier jour une variété de produits de haute qualité et abordables.',
    'ourPassion'	=> 'Notre passion pour en excellence nous a guidés depuis le début et continue de nous conduire vers avenir. En équipe de Stems Flower sait que chaque produit compte et se efforce de rendre expérience de magasinage aussi enrichissante et amusante que possible. Consultez notre boutique et nos offres spéciales et contactez-nous pour toute question ou demande.',
    // --------------------------------- Masters
    'account'	=> 'Compte',
    'shoppingCart'	=> 'Le chariot',
    'articles'	=> ' articles',
    'home'	=> 'Maison',
    'aboutnav'	=> 'À propos de nous',
    'services'	=> 'Services',
    'contact'	=> 'Contacter',
 
);
?>