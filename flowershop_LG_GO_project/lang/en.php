<?php


return array(
 
    // --------------------------------- About
    'about'	=> 'About Us',
    'story'	=> 'The Story of Stems Flower',
    'business'	=> 'Best in the Business',
    'stemsFlower'	=> 'Stems Flower is a well-renowned online store that has continually featured a variety of high-quality and affordable products since day one.',
    'ourPassion'	=> 'Our passion for excellence has driven us from the beginning, and continues to drive us into the future. The team at Stems Flower knows that every product counts, and strives to make the entire shopping experience as rewarding and fun as possible. Check out our store and special offers, and get in touch with questions or requests.',
    // --------------------------------- Masters
    'account'	=> 'Account',
    'shoppingCart'	=> 'Shopping Cart',
    'articles'	=> ' items',
    'home'	=> 'Home',
    'aboutnav'	=> 'About',
    'services'	=> 'Services',
    'contact'	=> 'Contact',


);
?>