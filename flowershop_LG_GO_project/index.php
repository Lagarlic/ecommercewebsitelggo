<?php

require_once "setup.php";
require_once "cart_order.php";

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;
  
function generateRandomString($length = 30) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
return $randomString;
}

$app->get('/', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    $dict = include "lang/$_SESSION[USER_LANGUAGE].php";

    $categoriesList =  DB::query("SELECT categories.categoryImagePath, cen.*  FROM categories , categorytranslatesen AS cen WHERE categories.categoryId = cen.categoryId");
    //print_r($categoriesList);
    return $view->render($response, 'en/index.html.twig', ['categoriesList' => $categoriesList, 'v' => $dict]);
});

/******************************************************************About************************************************************ */

$app->get('/about', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    $dict = include "lang/$_SESSION[USER_LANGUAGE].php";

    return $view->render($response, 'en/about.html.twig', ['v' => $dict]); 
});


// ------------------------------------------------------------------- login --------------------------------------------------------------------
//Stage 1: show form
$app->get('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    $dict = include "lang/$_SESSION[USER_LANGUAGE].php";

    return $view->render($response, 'en/login.html.twig', ['v' => $dict]);
});

//Stage 2&3: receiving submission
$app->post('/login', function (Request $request, Response $response, array $args) {
    global $log;
    //print_r($_POST);

    $view = Twig::fromRequest($request);
    //extract submitted values
    $postVars = $request->getParsedBody();
    $email = $postVars['email'];
    $password = $postVars['password'];
    

    $loginSuccessful = false;
    $errorsArray = array();
    
    //check validility
    $result = DB::queryFirstRow("SELECT * FROM customers WHERE email=%s LIMIT 1", $email);
    
    if ($result) {      
        $passEnc = hash('sha256', $password, false); //encrypt password 
        //echo $passEnc;      
        //if ($result['password'] == $password && $result['verified'] == 1) {
        if ($result['password'] == $passEnc && $result['verified'] == 1) {
            $loginSuccessful = true;
        }else {
            array_push($errorsArray, "Login failed");
        }
    }
  
    if (!$loginSuccessful) {
        return $view->render($response, 'en/login.html.twig', [
            'v' => $postVars, 'errorsArray' => $errorsArray
        ]);
    } 
    
    else {      
        
        $log->debug(sprintf("User logged in: email=%s", $email));
        unset($result['password']); //remove passowrd from array
        $_SESSION['user'] = $result;
        //print_r($_SESSION);
        return $view->render($response, 'en/login_success.html.twig', ['v' => $result , 'userSession' => $_SESSION['user']]);
    }
    
});
// ------------------------------------------------------------------- end login --------------------------------------------------------------------

//------------------------------------------------------------------- fb login --------------------------------------------------------------------
$app->post('/do/reg/fb', function (Request $request, Response $response, array $args) {
    
    //$view = Twig::fromRequest($request);
    
    $json = $_POST['param'];
    $array = json_decode($json, true);
    
    $uidFb = $array['authResponse']['userID'];
    $tokenFb = $array['authResponse']['accessToken'];
    
    $loginSuccessful = false;
    $result = DB::queryFirstRow("SELECT * FROM customers WHERE email=%s LIMIT 1", $uidFb);
    
    if ($result) {             
            $loginSuccessful = true;
            $_SESSION['user'] = $result;
            
    }
    
    if (!$result) {             
        
        if ($array['status'] === 'connected') {
            if($uidFb) {
                 
                //DB::insert('customers', [ 'lastName' => '', 'firstName' => '', 'email' => $email, 'password' => '', 'phoneNo' => '', 'street' => '', 'city' => '', 'province' => '', 'country' => '', 'postalCode' => '', 'verified' => 1, 'token' => $tokenFb]);
                DB::insert('customers', [ 'email' => $uidFb, 'verified' => 1, 'token' => $tokenFb]);
                $newId = DB::insertId();
                
                
                $loginSuccessful = true;
                $result = DB::queryFirstRow("SELECT * FROM customers WHERE email=%s LIMIT 1", $uidFb);
                $_SESSION['user'] = $result;
                
                
                //$log->debug(sprintf("User registered: id=%d, email=%s", $newId, $email));    
            }
        }
        
    }
    
    $response = $response->withStatus(201); // record created
    $response->getBody()->write(json_encode($newId));
    return $response;
    
   
});
//------------------------------------------------------------------- end  fb login --------------------------------------------------------------------

//------------------------------------------------------------------- logout --------------------------------------------------------------------
$app->get('/logout', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    unset($_SESSION['user']);
    return $response->withHeader('Location', '/');
});
// ------------------------------------------------------------------- end logout --------------------------------------------------------------------

// ---------------------------------------------------------------------- register --------------------------------------------------------

// ----  email confirmation

$app->get('/register/confirmation/{token}', function (Request $request, Response $response, array $args) {
    global $log;
    $view = Twig::fromRequest($request);
    // this needs to be done both for get and post
    $token = $args['token'];
    //print_r($token);
    
    $confirmCustomer = DB::queryFirstRow("SELECT * FROM customers WHERE token=%s", $token);
    //print_r($confirmCustomer);

    if (!$confirmCustomer) {
        $log->debug(sprintf('Confirmation token not found, token=%s', $confirmCustomer));
        return $view->render($response, 'en/register_mail_confirmation_error.html.twig');
    }

    // update customer

    DB::update('customers', ['verified' => 1,'token' => null], "customerId=%d", $confirmCustomer['customerId']);
    
    return $view->render($response, 'en/register_mail_confirmation_success.html.twig');
    
});
// ----  end email confirmation

//Stage 1: show form
$app->get('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    $dict = include "lang/$_SESSION[USER_LANGUAGE].php";

    return $view->render($response, 'en/register.html.twig', ['v' => $dict]);
});

//Stage 2&3: receiving submission
$app->post('/register', function (Request $request, Response $response, array $args) {
    global $log;
    $view = Twig::fromRequest($request);
    //extract submitted values
    $postVars = $request->getParsedBody();
    $firstName = $postVars['firstName'];
    $lastName = $postVars['lastName'];
    $email = $postVars['email'];
    $pass1 = $postVars['pass1'];
    $pass2 = $postVars['pass2'];
    //--------------------------------
    $phoneNo = $postVars['phoneNumber'];
    $street = $postVars['street'];
    $city = $postVars['city'];
    $province = $postVars['province'];
    $country = $postVars['country'];
    $postalCode = $postVars['postalCode'];
    $token = generateRandomString();

    //check validility
    $errorsArray = array();
    if (preg_match('/^[a-zA-Z][a-zA-Z0-9_]{1,19}$/', $firstName) != 1) {
        array_push($errorsArray, "First name must be 2-20 characters long, begin with a letter and only "
            . "consist of uppercase/lowercase letters, digits, and underscores");
        $postVars['username'] = "";
    }
    if (preg_match('/^[a-zA-Z][a-zA-Z0-9_]{1,19}$/', $lastName) != 1) {
        array_push($errorsArray, "Last name must be 2-20 characters long, begin with a letter and only "
            . "consist of uppercase/lowercase letters, digits, and underscores");
        $postVars['username'] = "";
    }

    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorsArray, "Email does not look valid.");
        //$postVars['email'] = "";
    } else {
        //Check email is not registered yet using Meekrodb
        $result = DB::queryFirstRow("SELECT * FROM customers WHERE email=%s LIMIT 1", $email);
        if ($result) {
            //print_r($result);
            array_push($errorsArray, "this email is already registered, try a different one.");
            $postVars['email'] = "";
        }
    }

    if ($pass1 != $pass2) {
        array_push($errorsArray, "Passwords do not match");
    } else {
        if ((strlen($pass1) < 6)
            || (preg_match("/[A-Z]/", $pass1) == FALSE)
            || (preg_match("/[a-z]/", $pass1) == FALSE)
            || (preg_match("/[0-9]/", $pass1) == FALSE)
        ) {
            array_push($errorsArray, "Password must be at least 6 characters long, "
                . "with at least one uppercase, one lowercase, and one digit in it");
        }
    }

    //
    if ($errorsArray) {
        //print_r($_POST);
        return $view->render($response, 'en/register.html.twig', [
            'v' => $postVars, 'errorsArray' => $errorsArray
        ]);
    } else {
        //Insert record to DB using Meekrodb
        //print_r($_POST);
        // encrypt password before insert
        $pass1 = hash('sha256', $pass1, false);
        DB::insert('customers', [ 'lastName' => $lastName, 'firstName' => $firstName, 'email' => $email, 'password' => $pass1, 'phoneNo' => $phoneNo, 'street' => $street, 'city' => $city, 'province' => $province, 'country' => $country, 'postalCode' => $postalCode, 'verified' => 0, 'token' => $token]);
        $newId = DB::insertId();
        $log->debug(sprintf("User registered: id=%d, email=%s", $newId, $email));
        //print_r($newId);
        //print_r($log);

       // $to  = $email; 

        $to  = $postVars['email']; 
        $subject = "Confirmation of registration"; 
        $message = "<p>Hello $firstName ! <a href='https://flowershop.ipd20.com/register/confirmation/{$token}'> Click to confirm your registration </a></p>";
       
        //print_r($to);

        $headers  = "Content-type: text/html; charset=windows-1251 \r\n"; 
        mail($to, $subject, $message, $headers); 
        //mail($to, $subject, $message);


        return $view->render($response, 'en/register_success.html.twig');
    }
});
// ---------------------------------------------------------------------- end register ------------------------------------------------------------------------

// ----------------------------------------------------------------- contact -----------------------------------------------------------------------------
$app->get('/contact', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    $dict = include "lang/$_SESSION[USER_LANGUAGE].php";

    $categoriesList =  DB::query("SELECT categories.categoryImagePath, cen.*  FROM categories , categorytranslatesen AS cen WHERE categories.categoryId = cen.categoryId");
    //print_r($categoriesList);
    return $view->render($response, 'en/contact.html.twig', ['categoriesList' => $categoriesList, 'v' => $dict]);
});
// ------------------------------------------------------------------ end contact ----------------------------------------------------------------------------

// -------------------------------------------------------------------- services ------------------------------------------------------------------------
// STATE 1: first show of the form
$app->get('/services', function (Request $request, Response $response) {

    $view = Twig::fromRequest($request);

    $dict = include "lang/$_SESSION[USER_LANGUAGE].php";

    $categoriesList =  DB::query("SELECT categories.categoryImagePath, cen.*  FROM categories , categorytranslatesen AS cen WHERE categories.categoryId = cen.categoryId");
    //print_r($categoriesList);
    return $view->render($response, 'en/services.html.twig', ['categoriesList' => $categoriesList, 'v' => $dict]);
});

// STATE 2&3: receiving submission
$app->post('/services', function (Request $request, Response $response, array $args) {

    $view = Twig::fromRequest($request);
    $categoriesList =  DB::query("SELECT categories.categoryImagePath, cen.*  FROM categories , categorytranslatesen AS cen WHERE categories.categoryId = cen.categoryId");
    // extract submitted values
    $errorsArray = array();
    $postVars = $request->getParsedBody();
    $email = $postVars['email'];

    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorsArray, "Email does not look valid. Try again !");
        //$postVars['email'] = "";
    }

    if ($errorsArray) {
        return $view->render($response, 'en/services.html.twig', ['categoriesList' => $categoriesList, 'v' => $postVars, 'errorsArray' => $errorsArray]);
    } else {
        //Insert record to DB using Meekrodb
        DB::insert('subscriptions', ['email' => $email]);
        return $view->render($response, 'en/subscribe_success.html.twig', ['categoriesList' => $categoriesList]);
    }
});

// ---------------------------------------------------------------------- end services ------------------------------------------------------------------------

// ----------------------------------------------------------------- policy -----------------------------------------------------------------------------
$app->get('/policy', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'en/policy.html.twig');
});
// ------------------------------------------------------------------ end policy ----------------------------------------------------------------------------


$app->run();
