<?php

use Slim\Factory\AppFactory;

use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;


require __DIR__ . '/vendor/autoload.php';

session_start();

//------------------------------------------------------- language
function SetLang($p1) {
    if (!in_array($p1, array('en', 'fr'))) $p1 = 'en';
    $_SESSION['USER_LANGUAGE'] = $p1;
}
if(!$_SESSION['USER_LANGUAGE']) SetLang(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2));

//$_SESSION['USER_LANGUAGE'] = 'en';
//exit($_SESSION['USER_LANGUAGE']);
//include "lang/$_SESSION[USER_LANGUAGE].php";
//print_r($_GET); 

if ( isset ( $_GET["lang"]) ) {

    $_SESSION['USER_LANGUAGE'] = $_GET['lang'];
}
//----------------------------------------------------------- end language



$app = AppFactory::create();


// // Define Custom Error Handler
// $forbiddenErrorHandler = function (
//     Psr\Http\Message\ServerRequestInterface $request,
//     \Throwable $exception,
//     bool $displayErrorDetails,
//     bool $logErrors,
//     bool $logErrorDetails
// ) use ($app) {
//     $response = $app->getResponseFactory()->createResponse();
//     // seems the followin can be replaced by your custom response
//     // $page = new Alvaro\Pages\Error($c);
//     // return $page->notFound404($request, $response);

//     return $response->withHeader('Location','/forbidden',404);
// };

// Add Error Middleware for 404 - not found handling
$errorMiddleware = $app->addErrorMiddleware(true, true, true);

$errorMiddleware->setErrorHandler(
    \Slim\Exception\HttpNotFoundException::class, 
        function () use ($app) {
            $response = $app->getResponseFactory()->createResponse();
            return $response->withHeader('Location','/error_notfound', 404);
        }
);


// create a log channel

$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

if (strpos($_SERVER['HTTP_HOST'], "ipd20.com") !== false) {
    // hosting on ipd20.com
    DB::$user = 'cp4966_flowershop';
    DB::$password = 'HCLmklZpxQXh3oCV';
    DB::$dbName = 'cp4966_flowershop';
} else { // local computer
    DB::$user = 'project_flowershop';
    DB::$password = 'HCLmklZpxQXh3oCV';
    DB::$dbName = 'project_flowershop';
    DB::$port = 3333;
}


DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params)
{
    header("Location: /error_internal", 500);

    global $log;
    $log->error("Database erorr[Connection]: " . $params['error']);

    if ($params['query']) {
        $log->error("Database error[Query]: " . $params['query']);
    }
    die();
}

// Create Twig
$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ . '/cache', 'debug' =>true]);

// Set Global variable($_SESSION)
$twig->getEnvironment()->addGlobal('session', $_SESSION);
$twig->getEnvironment()->addGlobal('userSession', isset($_SESSION['user']) ? $_SESSION['user'] : false); 
$_SESSION['id'] = session_id();
$twig->getEnvironment()->addGlobal('sessionId', isset($_SESSION['id']) ? $_SESSION['id'] : false);
$twig->getEnvironment()->addGlobal('categoriesList', DB::query( "SELECT * FROM categorytranslatesen"));
$twig->getEnvironment()->addGlobal('totalItems', DB::queryFirstField( "SELECT SUM(quantity) FROM cartitems WHERE sessionId = %s", session_id()));


// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

/**************************************************************************************** */
//For testing - delete later
$app->get('/problem', function (Request $request, Response $response, array $args) {
    DB::query("SELECT blah FROM blah");
});

$app->get('/forbidden', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'en/error_forbidden.html.twig');
});

$app->get('/error_internal', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'en/error_internal.html.twig');
});

// for debugging purposes only
$app->get('/session', function (Request $request, Response $response, array $args) {
    $body = "<pre>\n\$_SESSION:\n" . print_r($_SESSION, true);
    $response->getBody()->write($body);
    return $response;
});

//PHP information
$app->get('/info', function (Request $request, Response $response, array $args) {    
    $info = phpinfo();
    $response->getBody()->write($info);
    return $response;
});