<?php


use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

require_once "setup.php";

//***** */ To use stripe: ******//
require_once('vendor/autoload.php');  // To use the stripe SDK for php
\Stripe\Stripe::setApiKey('sk_test_BGzlPMyPmisQ6TzseiScCaQx00QRUXwgTg'); // server key (secret key) from the account created in stripe    
//*************//

/******************************************************************CATEGORY************************************************************ */
const PRODUCTS_PER_PAGE = 18;
function isInteger($input)
{
    return (ctype_digit(strval($input)));
}

// +++ PAGINATION USING AJAX
$app->get('/ajax/productpage/{id}/{pageNo:[0-9]+}[/{minPrice:[0-9]+}/{maxPrice:[0-9]+}]', function (Request $request, Response $response, array $args) {
    //$app->get('/ajax/productpage/{id}/{pageNo:[0-9]+}[/{minPrice:[0-9]+}/{maxPrice:[0-9]+}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    //$get = $request->getQueryParams();
    $minPrice =  isset($args['minPrice']) ? ($args['minPrice']) : 0;
    $maxPrice =  isset($args['maxPrice']) ? ($args['maxPrice']) : 10000;
    //echo $minPrice;
    //echo $maxPrice;
    $categories = DB::query("SELECT * FROM categories");
    $categoryId = $args['id'];

    if (
        !isInteger($categoryId)
        || $categoryId < 1
        || $categoryId > count($categories)
    ) {
        return $response->withHeader("Location", "/forbidden", 403);
    }

    $pageNo = $args['pageNo'];
    $productSkip = ($pageNo - 1) * PRODUCTS_PER_PAGE;
    //$productsList = DB::query("SELECT * FROM products ORDER BY id LIMIT %i,%i ", $productSkip, PRODUCTS_PER_PAGE);
    $productsList = DB::query(
        "SELECT p.*, prten.*
        FROM products AS p
        JOIN producttranslatesen AS prten 
        ON p.productId = prten.productId
        WHERE p.categoryId = %i0 AND unitPrice > %d1 AND unitPrice<%d2
        LIMIT %i3,%i4 ",
        $categoryId,
        $minPrice,
        $maxPrice,
        $productSkip,
        PRODUCTS_PER_PAGE
    );
    $totalProducts = DB::queryFirstField("SELECT COUNT(*) AS 'count' FROM products WHERE products.categoryId = %i0 AND unitPrice > %d1 AND unitPrice<%d2", $categoryId, $minPrice, $maxPrice);

    $totalPages = ceil($totalProducts / PRODUCTS_PER_PAGE);
    return $view->render($response, '/en/ajax_productpage.html.twig', ['productsList' => $productsList, 'totalPages' => $totalPages]);
});

// ajax pagination index
$app->get('/category/{id}[/{minPrice:[0-9]+}/{maxPrice:[0-9]+}]', function (Request $request, Response $response, array $args) {
    //$app->get('/ap/productpage/{id}[/{minPrice:[0-9]+}/{maxPrice:[0-9]+}]', function (Request $request, Response $response, array $args) {

    $view = Twig::fromRequest($request);
    $categories = DB::query("SELECT * FROM categories");
    $categoryId = $args['id'];

    $minPrice =  isset($args['minPrice']) ? ($args['minPrice']) : 0;
    $maxPrice =  isset($args['maxPrice']) ? ($args['maxPrice']) : 10000;

    if (
        !isInteger($categoryId)
        || $categoryId < 1
        || $categoryId > count($categories)
    ) {
        return $response->withHeader("Location", "/forbidden", 403);
    }

    $totalProducts = DB::queryFirstField("SELECT COUNT(*) AS 'count' FROM products WHERE products.categoryId = %i0 AND unitPrice > %d1 AND unitPrice<%d2", $categoryId, $minPrice, $maxPrice);

    $totalPages = ceil($totalProducts / PRODUCTS_PER_PAGE);

    $dict = include "lang/$_SESSION[USER_LANGUAGE].php";

    return $view->render($response, '/en/category.html.twig', [
        'totalPages' => $totalPages, 'v' => $dict
    ]);
});

// --- PAGINATION USING AJAX
/* ******************************************************************************************************************************************************** */

//ajax add car item
$app->post('/addcartitem', function (Request $request, Response $response, array $args) {
    global $log;
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $json = $request->getBody();
    $data = json_decode($json, true); // true makes it return an associative array instead of an object


    $productId = $data['productId'];
    $sessionId = session_id();
    $quantity = $data['quantity'];
    $message = false;
    $cartItem = DB::queryFirstRow(
        "SELECT * 
            FROM cartitems 
            WHERE sessionId = %s
            AND productId = %i",
        $sessionId,
        $productId
    );

    if (isset($cartItem['quantity'])) {
        $quantity += $cartItem['quantity'];
        DB::insertUpdate(
            "cartitems",
            [
                'id' => $cartItem['id'],
                'sessionId' => $sessionId,
                'productId' => $productId,
                'quantity' => $quantity
            ],
            'quantity=%i',
            $quantity
        );
        $message = true;
        // $response->getBody()->write($message);
        // return $response;
    } else {
        DB::insert('cartitems', ['sessionId' => $sessionId, 'productId' => $productId, 'quantity' => $quantity]);
        $message = true;
        // $response->getBody()->write($message);
        // return $response;
    }
    if (!$message) {
        $response = $response->withStatus(403); // FIXME: should really be 401 instead, but not for JS
        $log->debug("GET /addcartitem refused 400 ");
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - add cart item failed"));
        return $response;
    }
    // $response->getBody()->write($message);
    //     return $response;
    $totalItems = DB::queryFirstField("SELECT SUM(quantity) FROM cartitems WHERE sessionId = %s", session_id());
    $json = json_encode($totalItems, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});

//******************************************************************* cart *************************************************/
$app->get('/cart', function (Request $request, Response $response, array $args) {    // 
    $view = Twig::fromRequest($request);
    $cartitemList = DB::query(
        "SELECT cartitems.id as id, cartitems.productID, quantity,
                     productName, productDescription, imageFilePath, unitPrice 
                     FROM cartitems JOIN products 
                        ON cartitems.productId = products.productId 
                     JOIN producttranslatesen  
                        ON products.productId = producttranslatesen.productId
                     WHERE sessionID=%s",
        session_id()
    );
    $subTotal = 0;
    foreach ($cartitemList as $value) {
        $subTotal += $value['unitPrice'] * $value['quantity'];
    }
    $shipping = 10;
    $taxes = 0.15;
    $total = ($subTotal + $shipping) * (1 + $taxes);

    $dict = include "lang/$_SESSION[USER_LANGUAGE].php";

    return $view->render(
        $response,
        'en/cart.html.twig',
        [
            'cartitemList' => $cartitemList,
            'subTotal' => $subTotal,
            'shipping' => $shipping,
            'taxes' => $taxes,
            'total' => $total,
            'v' => $dict


        ]
    );
});


$app->get('/cart/delete/{id:[0-9]+}',  function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($args['id'])) {
        $response = $response->withStatus(404);
        return $view->render($response, 'en/error_internal.html.twig');
    }
    $cartitemID = $args['id'];
    DB::delete('cartitems', 'cartitems.ID=%d AND cartitems.sessionID=%s', $cartitemID, session_id());
    return $response->withHeader('Location', '/cart');
});

//using AJAX
$app->get('/cart/update',  function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $get = $request->getQueryParams();
    if (isset($get['quantity']) && isset($get['cartItemId'])) {

        $cartItemId = $get['cartItemId'];
        $quantity = $get['quantity'];
        if ($quantity <= 0) {
            $response = $response->withStatus(404);
            return $view->render($response, 'en/error_notfound.html.twig');
        } else {
            DB::insertUpdate(
                "cartItems",
                [
                    'id' => $cartItemId,
                    'quantity' => $quantity
                ],
                'quantity=%i',
                $quantity
            );
        }
    }
    return $response->withHeader('Location', '/cart');
});

//****************************************************************** */ order handling ***********************************************/
//

$app->map(['GET', 'POST'], '/order', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    $cartitemList = DB::query(
        "SELECT cartitems.id as id, cartitems.productID, quantity,
         productName, productDescription, imageFilePath, unitPrice 
         FROM cartitems JOIN products 
            ON cartitems.productId = products.productId 
         JOIN producttranslatesen  
            ON products.productId = producttranslatesen.productId
         WHERE sessionID=%s",
        session_id()
    );
    $subTotal = 0;
    foreach ($cartitemList as $value) {
        $subTotal += $value['unitPrice'] * $value['quantity'];
    }
    $shipping = 10;
    $taxes = 15;
    $total = ($subTotal + $shipping) * (1 + $taxes / 100);

    if ($request->getMethod() == 'GET') {
        return $view->render($response, 'en/order.html.twig', [
            'subTotal' => number_format($subTotal, 2),
            'shipping' => number_format($shipping, 2),
            'taxes' => number_format($taxes, 2),
            'total' => number_format($total, 2)
        ]);
    } else { // POST - order form submitted

        $postVars = $request->getParsedBody();
        $customerName = $postVars['customerName'];
        $shipEmail = $postVars['shipEmail'];
        $shipTo = $postVars['shipTo'];
        $shipPostalCode = $postVars['shipPostalCode'];
        $shipPhoneNo = $postVars['shipPhoneNo'];
        $shipProvince = $postVars['shipProvince'];
        $shipCountry = $postVars['shipCountry'];
        $token = $postVars['stripeToken'];   // token given by stripe if credit card info is approved        
        $valueList = array(
            'customerName' => $customerName,
            'shipEmail' => $shipEmail,
            'shipTo' => $shipTo,
            'shipPostalCode' => $shipPostalCode,
            'shipProvince' => $shipProvince,
            'shipCountry' => $shipCountry,
            'shipPhoneNo' => $shipPhoneNo
        );
        // FIXME: verify inputs - MUST DO IT IN A REAL SYSTEM
        $errorList = array();
        //
        if ($errorList) {
            return $view->render($response, 'en/order.html.twig', [
                'subTotal' => number_format($subTotal, 2),
                'shipping' => number_format($shipping, 2),
                'taxes' => number_format($taxes, 2),
                'total' => number_format($total, 2),
                'v' => $valueList
            ]);
        } else { // SUCCESSFUL SUBMISSION
            //PAYMENT
            if (isset($token)) {
                // To be able to charge:               

                // Create Customer In Stripe
                $stripeCust = \Stripe\Customer::create(array("email" => $shipEmail, "source" => $token, "description" => "Flower sore- Stems Florist"));  // TODO. Add cardholder's name
                $stripeCustId = $stripeCust['id'];
                // Create a user in DB                
                DB::insert('users', [
                    'name' => $customerName,
                    'email' => $shipEmail,
                    'stripeCustId' => $stripeCustId
                ]);

                // Charge Customer
                $charge = \Stripe\Charge::create(array(
                    "amount" => $total * 100,                                           // price of the product. In Stripe we dont put the '.', so in this case 5000 = $50.00
                    "currency" => "cad",                                        // cad, usd, etc
                    "description" => "Flower sore- Stems Florist",       // not required, but will show on the back end
                    "customer" => $stripeCustId                                 // customer who bought it                    
                ));

                // Define some variables:
                $tid = $charge['id'];                   // transaction Id                

                // Add the transaction to DB
                DB::insert('transactions', [
                    'id' => $tid,
                    'userSCid' => $stripeCustId,
                    'product' => $charge['description'],
                    'amount' => $charge['amount'],
                    'currency' => $charge['currency'],
                    'status' => $charge['status']
                ]);

                //end payment test block

                DB::$error_handler = FALSE;
                DB::$throw_exception_on_error = TRUE;
                // PLACE THE ORDER
                try {
                    DB::startTransaction();
                    // 1. create summary record in 'orders' table (insert)
                    DB::insert('orderheaders', [
                        'customerID' => isset($_SESSION['user']) ? $_SESSION['user']['customerId'] : NULL,
                        'customerName' => $customerName,
                        'shipEmail' => $shipEmail,
                        'shipTo' => $shipTo,
                        'shipPostalCode' => $shipPostalCode,
                        'shipProvince' => $shipProvince,
                        'shipCountry' => $shipCountry,
                        'shipPhoneNo' => $shipPhoneNo,
                        'subTotal' => number_format($subTotal, 2),
                        'shipping' => number_format($shipping, 2),
                        'taxes' => number_format($taxes, 2),
                        'total' => number_format($total, 2),
                        'status' => $charge['status']
                    ]);
                    $orderID = DB::insertId();
                    // 2. copy all records from cartitems to 'orderitems' (select & insert)
                    $cartitemList = DB::query(
                        "SELECT cartitems.productID, quantity, unitPrice 
                    FROM cartitems, products WHERE cartitems.productID = products.productID AND sessionID=%s",
                        session_id()
                    );
                    // add orderID to every sub-array (element) in $cartitemList
                    array_walk($cartitemList, function (&$item, $key) use ($orderID) {
                        $item['orderID'] = $orderID;
                    });

                    DB::insert('orderitems', $cartitemList);
                    // 3. delete cartitems for this user's session (delete)
                    DB::delete('cartitems', "sessionID=%s", session_id());
                    DB::commit();
                    // Confirmation email will be send by stripe automatecly in Live mode                    
                    //
                    $totalItems = DB::queryFirstField("SELECT SUM(quantity) FROM cartitems WHERE sessionId = %s", session_id());
                    return $view->render($response, 'en/order_success.html.twig', ['totalItems' => $totalItems, 'tid' => $tid, 'amount' => $total]);
                } catch (MeekroDBException $e) {
                    DB::rollback();
                    db_error_handler(array(
                        'error' => $e->getMessage(),
                        'query' => $e->getQuery()
                    ));
                }
            }
        }
    }
});

//******************************************************************* cart *************************************************/
$app->get('/myorders', function (Request $request, Response $response, array $args) {    // 
    $view = Twig::fromRequest($request);
    $email = $_SESSION['user']['email'];
    //echo $email;
    $myoredrList = DB::query(
        "SELECT orderheaders.status, orderheaders.orderDate, orderitems.quantity,
            producttranslatesen.productName, products.imageFilePath,
            orderitems.unitPrice 
            FROM orderheaders JOIN orderitems 
            ON orderitems.orderId = orderheaders.orderId
            JOIN products 
            ON orderitems.productId = products.productId 
            JOIN producttranslatesen  
            ON products.productId = producttranslatesen.productId 
            WHERE orderheaders.shipEmail = %s0
            ORDER BY orderheaders.orderDate desc" , $email     
            );
    
    return $view->render( $response,'en/myorders.html.twig',['myoredrList' => $myoredrList ] );
});
