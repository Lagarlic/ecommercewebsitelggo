-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: May 18, 2020 at 03:38 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_flowershop`
--

-- --------------------------------------------------------

--
-- Table structure for table `cartitems`
--

CREATE TABLE `cartitems` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `sessionId` varchar(100) NOT NULL,
  `addedTS` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cartitems`
--

INSERT INTO `cartitems` (`id`, `productId`, `sessionId`, `addedTS`, `quantity`) VALUES
(1015, 2, 'n9be4j0v8qv5v8v2dhonb4bq9l', '2020-05-12 00:45:11', 2),
(1016, 3, 'n9be4j0v8qv5v8v2dhonb4bq9l', '2020-05-12 00:06:08', 1),
(1018, 1, 'n9be4j0v8qv5v8v2dhonb4bq9l', '2020-05-12 00:45:17', 1),
(1019, 2, '2nn8jijnb46d8snqokgpahooh1', '2020-05-12 05:12:50', 1),
(1020, 1, '2nn8jijnb46d8snqokgpahooh1', '2020-05-12 05:12:56', 1),
(1065, 2, '31ai211g1mskuqq4qpn8ri0n1p', '2020-05-17 03:57:11', 3),
(1066, 3, '31ai211g1mskuqq4qpn8ri0n1p', '2020-05-17 05:57:02', 4),
(1067, 5, '31ai211g1mskuqq4qpn8ri0n1p', '2020-05-17 05:56:09', 3),
(1069, 10, '31ai211g1mskuqq4qpn8ri0n1p', '2020-05-17 05:51:05', 1),
(1070, 1, '31ai211g1mskuqq4qpn8ri0n1p', '2020-05-17 05:54:23', 1),
(1071, 19, '31ai211g1mskuqq4qpn8ri0n1p', '2020-05-17 05:55:56', 1),
(1088, 16, '31ai211g1mskuqq4qpn8ri0n1p', '2020-05-17 12:58:26', 1),
(1105, 15, 'rvo5mb21tgrngr0afu9ocj9kks', '2020-05-18 05:51:25', 4),
(1106, 11, 'rvo5mb21tgrngr0afu9ocj9kks', '2020-05-18 05:53:05', 2);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `categoryId` int(11) NOT NULL,
  `categoryImagePath` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categoryId`, `categoryImagePath`) VALUES
(1, '/images/art-artistic-beautiful-bloom-311458.jpg'),
(2, '/images/blossom-bouquet-bouquets-colors-250716.jpg'),
(3, '/images/abstract-beautiful-beauty-bright-217763.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `categorytranslatesen`
--

CREATE TABLE `categorytranslatesen` (
  `categoryTranslateId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `categoryName` varchar(25) NOT NULL,
  `categoryDescription` varchar(5000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categorytranslatesen`
--

INSERT INTO `categorytranslatesen` (`categoryTranslateId`, `categoryId`, `categoryName`, `categoryDescription`) VALUES
(1, 1, 'Succulents', 'Succulents and cacti are low maintenance, water wise plants that store water in their leaves, stems or roots, creating a plump or succulent appearance. They are often found in hot, arid climates such as the desert and have adapted to tolerate long periods of drought. There are many varieties of succulents and cacti that come from all over the world. For best results each plant has individual needs, but there are general rules for succulent and cactus plant care. \r\n\r\nWater - If your container has drainage holes, water thoroughly once a week during active growth period. If your container does not have drainage holes, water sparingly to moisten soil but be sure water does not pool up at the bottom of container which can cause rotting. Allow soil to dry between waterings.\r\n\r\nLight - Place plant in a brightly lit south facing window indoors or an area with bright, indirect light outdoors. Some plants can tolerate full sun, but must be gradually acclimated to prevent sunburn.  If the light source is inadequate, etiolation will occur and your plant will become leggy as it stretches out towards a light source. \r\n\r\nSoil - Succulents and cacti like soil that is well aerated and fast draining. Perlite or pumice mixed with soil work well for this, or you can pick up cactus/succulent mix from your local nursery. \r\n\r\nTips:\r\n\r\nLithop Care (living rock) - Take special care not to overwater lithops, or they will rot.  Water during fall (when you see flower buds appear) and spring (after leaf shedding has occurred) thoroughly (until water runs through drainage holes) and let soil dry between waterings. Refrain from watering at all during winter and summer, save for very sparse sprinklings once a month.  Keep your lithop in a bright, south facing window. For more information visit lithops.info\r\n\r\nNutrition - Fertilize during growing season with a 10-10-10 fertilizer diluted to 1/4 strength each watering. \r\n\r\nColors - In general greener succulents are more tolerant to low light environments. If your space does not have a good light source, stay away from succulents with blue, purple, pink and white tones. \r\n\r\nPropagating - Succulents have many methods for reproduction and can propagate from cuttings, leaf cuttings and producing pups and seeds.  \r\n\r\nArtificial lighting - Succulents do best in natural light, but if this not available (during winter months or depending on your geographical location), you can supplement their light source with artificial grow lights. There are many options for energy efficient artificial lighting available.'),
(2, 2, 'Bouquets', 'Keep your floral arrangement looking fresh by following the tips below!\r\n\r\n- Give the stems a fresh cut and change out the water every few days. Use flower food if you have it. \r\n\r\n- Place your arrangement in the refrigerator at night.\r\n\r\n- Display in a cool area and avoid direct sunlight or heated air. '),
(3, 3, 'Houseplants', 'The secret to keeping a houseplant alive is to replicate its natural growing zone by giving it the amount of humidity, light, and water it prefers. Most houseplants fall into two categories, tropical (including ferns, palms, vines) or succulent (such as varieties of aloe, aeonium, and echeveria). The most popular houseplant of this decade, the fiddle-leaf fig, would rather be living in a West African rain forest than in your living room, so prepare to coddle it.\r\n\r\nWhere to start when it comes to houseplant care? Our field guides and expert posts offer tips and advice useful to both the novice and the veteran. Two rules of thumb for houseplant care: tropical plants like humidity (think: steamy bathroom) and succulents like dry warmth.');

-- --------------------------------------------------------

--
-- Table structure for table `categorytranslatesfr`
--

CREATE TABLE `categorytranslatesfr` (
  `categoryTranslateId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `categoryName` varchar(25) NOT NULL,
  `categoryDescription` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customerId` int(11) NOT NULL,
  `lastName` varchar(15) NOT NULL,
  `firstName` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(25) NOT NULL,
  `phoneNo` varchar(15) NOT NULL,
  `street` varchar(100) DEFAULT NULL,
  `city` varchar(15) NOT NULL,
  `province` varchar(15) NOT NULL,
  `country` varchar(15) NOT NULL,
  `postalCode` varchar(7) DEFAULT NULL,
  `verified` int(11) NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customerId`, `lastName`, `firstName`, `email`, `password`, `phoneNo`, `street`, `city`, `province`, `country`, `postalCode`, `verified`, `token`) VALUES
(34, 'Gilfanov', 'Oleg', 'gilfanovola@gmail.com', 'qwertyQ1', '4389226605', '2525 rue Gamache', 'Longueuil', 'Quebec', 'Canada', NULL, 1, ''),
(35, 'Marry', 'Marry', 'marry@marry.com', 'Marry123', '123123', 'dfr', 'Verdun', 'QC', 'Canada', 'h1h1h1', 0, 'iryWmUysWtWLGIn5vawpeDAj2LbP1x');

-- --------------------------------------------------------

--
-- Table structure for table `orderheaders`
--

CREATE TABLE `orderheaders` (
  `orderId` int(11) NOT NULL,
  `customerId` int(11) DEFAULT NULL,
  `customerName` varchar(100) NOT NULL,
  `orderDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `shipTo` varchar(30) NOT NULL,
  `shipEmail` varchar(256) NOT NULL,
  `shipPhoneNo` varchar(15) NOT NULL,
  `shipCity` varchar(15) NOT NULL,
  `shipProvince` varchar(12) NOT NULL,
  `shipCountry` varchar(20) NOT NULL,
  `shipPostalCode` int(11) NOT NULL,
  `status` enum('succeeded','shipped','delivered','failed','pending') NOT NULL DEFAULT 'pending',
  `shipping` decimal(10,2) NOT NULL,
  `subTotal` decimal(10,2) NOT NULL,
  `taxes` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orderheaders`
--

INSERT INTO `orderheaders` (`orderId`, `customerId`, `customerName`, `orderDate`, `shipTo`, `shipEmail`, `shipPhoneNo`, `shipCity`, `shipProvince`, `shipCountry`, `shipPostalCode`, `status`, `shipping`, `subTotal`, `taxes`, `total`) VALUES
(4, NULL, ' aswdef', '2020-05-10 00:19:55', '123street', 'tommy@tommy.tr', '123', '', 'QC', 'Canada', 0, 'pending', '10.00', '180.00', '15.00', '218.50'),
(5, NULL, 'rt ', '2020-05-10 00:27:20', '123street', 'tommy@tommy.tr', '123', '', 'QC', 'Canada', 0, 'pending', '10.00', '188.00', '15.00', '227.70'),
(7, NULL, 'Oleg Gilfanov', '2020-05-11 17:30:42', '2525 rue Gamache', 'gilfanovola@gmail.com', '4389226605', '', 'Quebec', 'Canada', 0, 'pending', '10.00', '159.00', '15.00', '194.35'),
(8, NULL, ' aswdef', '2020-05-13 03:08:03', '123street', 'tommy@tommy.tr', '123', '', 'QC', 'Canada', 0, 'pending', '10.00', '240.00', '15.00', '287.50'),
(9, NULL, ' ', '2020-05-13 04:09:41', '', '', '', '', '', '', 0, 'pending', '10.00', '94.00', '15.00', '119.60'),
(10, NULL, ' ', '2020-05-13 04:24:30', '', '', '', '', '', '', 0, 'pending', '10.00', '25.00', '15.00', '40.25'),
(11, NULL, ' aswdef', '2020-05-16 16:03:03', '123street', 'tommy@tommy.tr', '123', '', 'QC', 'Canada', 0, 'pending', '10.00', '177.00', '15.00', '215.05'),
(12, NULL, ' rety', '2020-05-16 20:17:37', 'kij', 'vseacinka@gmail.com', '123', '', 'jjj', 'okljg', 0, 'pending', '10.00', '94.00', '15.00', '119.60'),
(13, NULL, ' Test', '2020-05-16 20:53:54', '', 'test@test.com', '', '', '', '', 0, 'pending', '10.00', '155.00', '15.00', '189.75'),
(15, NULL, ' Test', '2020-05-16 20:57:21', '', 'test@test.com', '', '', '', '', 0, 'pending', '10.00', '65.00', '15.00', '86.25'),
(16, NULL, 'Test not log in', '2020-05-16 23:50:11', '', 'tommy@tommy.tr', '123', '', '', '', 0, 'pending', '10.00', '35.00', '15.00', '51.75'),
(17, NULL, 'Blabla', '2020-05-16 23:57:57', '', 'blalala@lala.la', '', '', '', '', 0, 'pending', '10.00', '65.00', '15.00', '86.25'),
(18, NULL, 'One more', '2020-05-17 00:10:07', 'er', 'hi@fgn.lo', '12345', '', 'ertg', 'erg', 0, 'pending', '10.00', '65.00', '15.00', '86.25'),
(19, NULL, 'Oleg Gilfanov', '2020-05-17 00:15:59', '2525 rue Gamache', 'gilfanovola@gmail.com', '4389226605', '', 'Quebec', 'Canada', 0, 'pending', '10.00', '113.00', '15.00', '141.45'),
(20, 34, 'Oleg Gilfanov', '2020-05-17 01:01:03', '2525 rue Gamache', 'gilfanovola@gmail.com', '4389226605', '', 'Quebec', 'Canada', 0, 'pending', '10.00', '22.00', '15.00', '36.80'),
(21, NULL, 'Qweer', '2020-05-17 01:04:29', 'sedbth', 'blalala@lala.la', '134', '', 'sdeth', 'dsetghn', 0, 'pending', '10.00', '65.00', '15.00', '86.25'),
(22, NULL, 'Svetic', '2020-05-17 01:25:14', '2525 rue Gamache', 'vseacinka@gmail.com', '4389226605', '', 'Quebec', 'Canada', 0, 'pending', '10.00', '65.00', '15.00', '86.25'),
(23, NULL, 'Tratata', '2020-05-17 01:32:59', '2525 rue Gamache', 'stripetest123@mailinator.com', '123', '', 'Quebec', 'Canada', 0, 'pending', '10.00', '39.00', '15.00', '56.35'),
(24, NULL, 'Trrrrra', '2020-05-17 02:13:37', 'kij', 'stripetest123@mailinator.com', '123', '', 'jjj', 'okljg', 0, 'pending', '10.00', '45.00', '15.00', '63.25'),
(25, NULL, 'Trrrrra', '2020-05-17 02:36:46', 'kij', 'stripetest123@mailinator.com', '123', '', 'jjj', 'okljg', 0, 'pending', '10.00', '15.00', '15.00', '28.75'),
(26, NULL, 'Trrrrra', '2020-05-17 02:39:44', 'kij', 'stripetest123@mailinator.com', '123', '', 'jjj', 'okljg', 0, 'pending', '10.00', '11.00', '15.00', '24.15'),
(27, NULL, 'Trrrrra', '2020-05-17 02:50:05', 'kij', 'stripetest123@mailinator.com', '132425', '', 'jjj', 'okljg', 0, 'pending', '10.00', '195.00', '15.00', '235.75'),
(28, NULL, 'Trrrrra', '2020-05-17 02:54:03', 'kij', 'stripetest123@mailinator.com', '132425', '', 'jjj', 'okljg', 0, 'pending', '10.00', '100.00', '15.00', '126.50'),
(29, NULL, 'Trrrrra', '2020-05-17 02:55:39', 'adgsf', 'stripetest123@mailinator.com', '123', '', 'aswef', 'asew', 0, 'pending', '10.00', '98.00', '15.00', '124.20'),
(30, NULL, 'Chiort', '2020-05-17 14:18:02', '2525 rue Gamache', 'chiort@cvgfxd.rt', '4389226605', '', 'Quebec', 'Canada', 0, 'pending', '10.00', '88.00', '15.00', '112.70'),
(31, 34, 'Oleg Gilfanov', '2020-05-17 14:24:36', '2525 rue Gamache', 'gilfanovola@gmail.com', '4389226605', '', 'Quebec', 'Canada', 0, 'pending', '10.00', '90.00', '15.00', '115.00'),
(32, 34, 'Oleg Gilfanov', '2020-05-17 14:45:56', '2525 rue Gamache', 'gilfanovola@gmail.com', '4389226605', '', 'Quebec', 'Canada', 0, 'pending', '10.00', '49.00', '15.00', '67.85'),
(33, NULL, 'Tom Bell', '2020-05-17 14:50:02', '2525 rue Gamache', 'stripetest123@mailinator.com', '4389226605', '', 'Quebec', 'Canada', 0, 'pending', '10.00', '25.00', '15.00', '40.25'),
(34, NULL, 'Test sattus', '2020-05-17 15:06:46', '2525 rue Gamache', 'blalala@lala.la', '4389226605', '', 'Quebec', 'Canada', 0, 'succeeded', '10.00', '35.00', '15.00', '51.75'),
(35, NULL, 'dff', '2020-05-18 00:41:27', '2525 rue Gamache', 'tommy@tommy.tr', '123', '', 'Quebec', 'Canada', 0, 'succeeded', '10.00', '439.00', '15.00', '516.35');

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `orderDetailsId` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `unitPrice` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orderitems`
--

INSERT INTO `orderitems` (`orderDetailsId`, `orderId`, `productId`, `unitPrice`, `quantity`) VALUES
(1, 4, 19, '25.00', 1),
(2, 4, 2, '45.00', 2),
(3, 4, 3, '65.00', 1),
(4, 5, 2, '45.00', 1),
(5, 5, 3, '65.00', 1),
(6, 5, 5, '39.00', 2),
(8, 7, 1, '49.00', 1),
(9, 7, 6, '55.00', 2),
(10, 8, 2, '45.00', 1),
(11, 8, 5, '39.00', 5),
(12, 9, 2, '45.00', 1),
(13, 9, 1, '49.00', 1),
(14, 10, 18, '25.00', 1),
(15, 11, 2, '45.00', 1),
(16, 11, 13, '11.00', 2),
(17, 11, 6, '55.00', 2),
(18, 12, 2, '45.00', 1),
(19, 12, 1, '49.00', 1),
(20, 13, 3, '65.00', 1),
(21, 13, 2, '45.00', 2),
(23, 15, 3, '65.00', 1),
(24, 16, 4, '35.00', 1),
(25, 17, 3, '65.00', 1),
(26, 18, 3, '65.00', 1),
(27, 19, 4, '35.00', 1),
(28, 19, 5, '39.00', 2),
(29, 20, 12, '11.00', 2),
(30, 21, 3, '65.00', 1),
(31, 22, 3, '65.00', 1),
(32, 23, 5, '39.00', 1),
(33, 24, 2, '45.00', 1),
(34, 25, 8, '15.00', 1),
(35, 26, 11, '11.00', 1),
(36, 27, 3, '65.00', 3),
(37, 28, 19, '25.00', 4),
(38, 29, 1, '49.00', 2),
(39, 30, 9, '49.00', 1),
(40, 30, 7, '20.00', 1),
(41, 30, 15, '19.00', 1),
(42, 31, 2, '45.00', 2),
(43, 32, 1, '49.00', 1),
(44, 33, 19, '25.00', 1),
(45, 34, 4, '35.00', 1),
(46, 35, 4, '35.00', 5),
(47, 35, 19, '25.00', 1),
(48, 35, 17, '25.00', 1),
(49, 35, 2, '45.00', 1),
(50, 35, 3, '65.00', 1),
(51, 35, 6, '55.00', 1),
(52, 35, 1, '49.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `productId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `unitPrice` decimal(10,2) NOT NULL,
  `imageFilePath` varchar(250) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`productId`, `categoryId`, `unitPrice`, `imageFilePath`) VALUES
(1, 2, '49.00', '/images/bouquets/bouquet1.jpg'),
(2, 2, '45.00', '/images/bouquets/bouquet2.jpg'),
(3, 2, '65.00', '/images/bouquets/bouquet3.jpg'),
(4, 2, '35.00', '/images/bouquets/bouquet4.jpg'),
(5, 2, '39.00', '/images/bouquets/bouquet5.jpg'),
(6, 2, '55.00', '/images/bouquets/bouquet6.jpg'),
(7, 2, '20.00', '/images/bouquets/lavender.jpg'),
(8, 2, '15.00', '/images/bouquets/narcissus.jpg'),
(9, 2, '49.00', '/images/bouquets/pink.jpg'),
(10, 2, '29.00', '/images/bouquets/pink-lily.jpg'),
(11, 2, '11.00', '/images/bouquets/purple-petal.jpg'),
(12, 2, '11.00', '/images/bouquets/purple-tulip.jpg'),
(13, 2, '11.00', '/images/bouquets/red-tulip.jpg'),
(14, 2, '29.00', '/images/bouquets/red-tulip-bouquet.jpg'),
(15, 2, '19.00', '/images/bouquets/sunflower1.jpg'),
(16, 2, '35.00', '/images/bouquets/white-and-red-tulip.jpg'),
(17, 1, '25.00', '/images/succulents/succulent1.jpg'),
(18, 1, '25.00', '/images/succulents/succulent2.jpg'),
(19, 1, '25.00', '/images/succulents/succulent3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `producttranslatesen`
--

CREATE TABLE `producttranslatesen` (
  `productTranslateId` int(11) NOT NULL,
  `productName` varchar(50) NOT NULL,
  `productDescription` varchar(250) DEFAULT NULL,
  `productId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `producttranslatesen`
--

INSERT INTO `producttranslatesen` (`productTranslateId`, `productName`, `productDescription`, `productId`) VALUES
(1, 'Bouquet', '', 1),
(2, 'Bouquet', NULL, 2),
(3, 'Bouquet of roses', NULL, 3),
(4, 'Bouquet of roses', NULL, 4),
(5, 'Oranges', NULL, 5),
(6, 'Bouquet', NULL, 6),
(7, 'Lavander', NULL, 7),
(8, 'Narcissus', NULL, 8),
(9, 'Pink bouquet', NULL, 9),
(10, 'Lily', NULL, 10),
(11, 'Petal', NULL, 11),
(12, 'Purple tulips', NULL, 12),
(13, 'Red tulips', NULL, 13),
(14, 'Red tulips', NULL, 14),
(15, 'Sunflowers', NULL, 15),
(16, 'Tulips bouquet', NULL, 16),
(17, 'Succulent', NULL, 17),
(18, 'Succulent', NULL, 18),
(19, 'Succulent', NULL, 19);

-- --------------------------------------------------------

--
-- Table structure for table `producttranslatesfr`
--

CREATE TABLE `producttranslatesfr` (
  `productTranslateId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productName` varchar(50) NOT NULL,
  `productDescription` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `email`) VALUES
(2, 'gilfanovolas@mail.ru'),
(3, 'gilfanrrrrrrrrrovola@gmai.com'),
(1, 'jggsd@lljg.jk');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` varchar(255) NOT NULL,
  `userSCid` varchar(255) NOT NULL,
  `product` varchar(255) DEFAULT NULL,
  `amount` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `transactionTS` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `userSCid`, `product`, `amount`, `currency`, `status`, `transactionTS`) VALUES
('ch_1Gjbh8Cg20b9A1h8CaYT3BC9', 'cus_HIBkQFjCDlgVCm', 'Flower sore- Stems Florist', '11500', 'cad', 'succeeded', '2020-05-16 21:53:39'),
('ch_1GjbN7Cg20b9A1h88r85F6Ld', 'cus_HIBkQFjCDlgVCm', 'Flower sore- Stems Florist', '5635', 'cad', 'succeeded', '2020-05-16 21:32:58'),
('ch_1GjbowCg20b9A1h8whcpLkzg', 'cus_HIBkQFjCDlgVCm', 'Flower sore- Stems Florist', '11500', 'cad', 'succeeded', '2020-05-16 22:01:44'),
('ch_1Gjbt7Cg20b9A1h8VgP1vLQj', 'cus_HIBkQFjCDlgVCm', 'Flower sore- Stems Florist', '6325', 'cad', 'succeeded', '2020-05-16 22:06:03'),
('ch_1Gjbw0Cg20b9A1h835juMVKZ', 'cus_HIBkQFjCDlgVCm', 'Flower sore- Stems Florist', '6325', 'cad', 'succeeded', '2020-05-16 22:09:01'),
('ch_1GjbxrCg20b9A1h81xAu4Gep', 'cus_HIBkQFjCDlgVCm', 'Flower sore- Stems Florist', '6325', 'cad', 'succeeded', '2020-05-16 22:10:57'),
('ch_1GjbypCg20b9A1h83EeGBPJi', 'cus_HIBkQFjCDlgVCm', 'Flower sore- Stems Florist', '6325', 'cad', 'succeeded', '2020-05-16 22:11:56'),
('ch_1Gjc0QCg20b9A1h8iafsJdI8', 'cus_HIBkQFjCDlgVCm', 'Flower sore- Stems Florist', '6325', 'cad', 'succeeded', '2020-05-16 22:13:35'),
('ch_1GjcdZCg20b9A1h8IoNOiUIR', 'cus_HIBkQFjCDlgVCm', 'Flower sore- Stems Florist', '12650', 'cad', 'succeeded', '2020-05-16 22:54:03'),
('ch_1Gjcf7Cg20b9A1h8zbIA6N5k', 'cus_HIBkQFjCDlgVCm', 'Flower sore- Stems Florist', '12420', 'cad', 'succeeded', '2020-05-16 22:55:39'),
('ch_1GjcMrCg20b9A1h8NFH7PYVK', 'cus_HIBkQFjCDlgVCm', 'Flower sore- Stems Florist', '2875', 'cad', 'succeeded', '2020-05-16 22:36:46'),
('ch_1GjcPiCg20b9A1h8NmTN7Hh1', 'cus_HIBkQFjCDlgVCm', 'Flower sore- Stems Florist', '2415', 'cad', 'succeeded', '2020-05-16 22:39:44'),
('ch_1GjcZjCg20b9A1h8qaydcPTT', 'cus_HIBkQFjCDlgVCm', 'Flower sore- Stems Florist', '23575', 'cad', 'succeeded', '2020-05-16 22:50:05'),
('ch_1GjnJVCg20b9A1h8kmxtgAur', 'cus_HIO5saA8Y8a7PH', 'Flower sore- Stems Florist', '11270', 'cad', 'succeeded', '2020-05-17 10:18:02'),
('ch_1GjnkVCg20b9A1h8M6wSHdOC', 'cus_HIOXZHQbd6PM8T', 'Flower sore- Stems Florist', '6785', 'cad', 'succeeded', '2020-05-17 10:45:56'),
('ch_1GjnoUCg20b9A1h8pIQ5ydK6', 'cus_HIObTDurMROt0V', 'Flower sore- Stems Florist', '4025', 'cad', 'succeeded', '2020-05-17 10:50:02'),
('ch_1GjnPrCg20b9A1h8AJW3IEaK', 'cus_HIOCxCu3NGksmn', 'Flower sore- Stems Florist', '11500', 'cad', 'succeeded', '2020-05-17 10:24:36'),
('ch_1Gjo4fCg20b9A1h84fgVIFE0', 'cus_HIOsG4NdbbKYtY', 'Flower sore- Stems Florist', '5175', 'cad', 'succeeded', '2020-05-17 11:06:46'),
('ch_1Gjx2oCg20b9A1h826waynBf', 'cus_HIY9PcPxPEcgK1', 'Flower sore- Stems Florist', '51635', 'cad', 'succeeded', '2020-05-17 20:41:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `stripeCustId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `stripeCustId`) VALUES
(1, 'tom@tom.com', 'Tom Tom', 'cus_HHr0eQz6SFUxuK'),
(2, 'marry@marry.com', 'Marry Marry', 'cus_HHqnRr2EkjpS1d'),
(3, 'test@test.com', 'Test', 'cus_HI7LJ8rCnO8D3z'),
(4, 'tommy@tommy.tr', 'Test not log in', NULL),
(5, 'blalala@lala.la', 'Blabla', 'cus_HIADNzrKdHnuEo'),
(6, 'hi@fgn.lo', 'One more', 'cus_HIAP0hzuwPZ17P'),
(7, 'gilfanovola@gmail.com', 'Oleg Gilfanov', 'cus_HIAV34SoDSdu1C'),
(9, 'gilfanovola@gmail.com', 'Oleg Gilfanov', NULL),
(10, 'gilfanovola@gmail.com', 'Oleg Gilfanov', 'cus_HIBEZsVid6PxsK'),
(11, 'blalala@lala.la', 'Qweer', 'cus_HIBIkxFHmJWPDX'),
(12, 'vseacinka@gmail.com', 'Svetic', 'cus_HIBcQBLKCwcCVT'),
(13, 'stripetest123@mailinator.com', 'Tratata', 'cus_HIBkQFjCDlgVCm'),
(14, 'stripetest123@mailinator.com', 'Invose check', 'cus_HIC5mszTtZeOyG'),
(15, 'stripetest123@mailinator.com', 'Tratata', 'cus_HICDFnGHDVex0m'),
(16, 'stripetest123@mailinator.com', 'Trrrrra', 'cus_HICHcbCIS7YxCX'),
(17, 'stripetest123@mailinator.com', 'Trrrrra', 'cus_HICKypJWI5wueK'),
(27, 'gilfanovola@gmail.com', 'Oleg Gilfanov', 'cus_HINgkgbBM1LArU'),
(28, 'gilfanovola@gmail.com', 'Oleg Gilfanov', 'cus_HINqR16GD0Orma'),
(29, 'gilfanovola@gmail.com', 'Oleg Gilfanov', 'cus_HINtRiuXLfshK5'),
(30, 'tommy@tommy.tr', 'test2', 'cus_HINuX0oB9hGBtp'),
(31, 'chiort@cvgfxd.rt', 'Chiort', 'cus_HIO5saA8Y8a7PH'),
(32, 'gilfanovola@gmail.com', 'Oleg Gilfanov', 'cus_HIO8tisncYdIiW'),
(33, 'gilfanovola@gmail.com', 'Oleg Gilfanov', 'cus_HIOCxCu3NGksmn'),
(34, 'gilfanovola@gmail.com', 'Oleg Gilfanov', 'cus_HIOXZHQbd6PM8T'),
(35, 'stripetest123@mailinator.com', 'Tom Bell', 'cus_HIObTDurMROt0V'),
(36, 'blalala@lala.la', 'Test sattus', 'cus_HIOsG4NdbbKYtY'),
(37, 'tommy@tommy.tr', 'dff', 'cus_HIY9PcPxPEcgK1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessionId` (`sessionId`),
  ADD KEY `productId` (`productId`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `categorytranslatesen`
--
ALTER TABLE `categorytranslatesen`
  ADD PRIMARY KEY (`categoryTranslateId`),
  ADD UNIQUE KEY `categoryId` (`categoryId`);

--
-- Indexes for table `categorytranslatesfr`
--
ALTER TABLE `categorytranslatesfr`
  ADD PRIMARY KEY (`categoryTranslateId`),
  ADD UNIQUE KEY `categoryId` (`categoryId`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customerId`),
  ADD UNIQUE KEY `email` (`email`) USING BTREE;

--
-- Indexes for table `orderheaders`
--
ALTER TABLE `orderheaders`
  ADD PRIMARY KEY (`orderId`),
  ADD KEY `customerId` (`customerId`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`orderDetailsId`),
  ADD KEY `orderId` (`orderId`),
  ADD KEY `productId` (`productId`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`productId`),
  ADD KEY `categoryId` (`categoryId`);

--
-- Indexes for table `producttranslatesen`
--
ALTER TABLE `producttranslatesen`
  ADD PRIMARY KEY (`productTranslateId`),
  ADD UNIQUE KEY `productId` (`productId`);

--
-- Indexes for table `producttranslatesfr`
--
ALTER TABLE `producttranslatesfr`
  ADD PRIMARY KEY (`productTranslateId`),
  ADD UNIQUE KEY `productId` (`productId`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_ibfk_1` (`userSCid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stripeCustId` (`stripeCustId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cartitems`
--
ALTER TABLE `cartitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1108;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categorytranslatesen`
--
ALTER TABLE `categorytranslatesen`
  MODIFY `categoryTranslateId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categorytranslatesfr`
--
ALTER TABLE `categorytranslatesfr`
  MODIFY `categoryTranslateId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `orderheaders`
--
ALTER TABLE `orderheaders`
  MODIFY `orderId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `orderDetailsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `productId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `producttranslatesen`
--
ALTER TABLE `producttranslatesen`
  MODIFY `productTranslateId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `producttranslatesfr`
--
ALTER TABLE `producttranslatesfr`
  MODIFY `productTranslateId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD CONSTRAINT `cartitems_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`);

--
-- Constraints for table `categorytranslatesen`
--
ALTER TABLE `categorytranslatesen`
  ADD CONSTRAINT `categorytranslatesen_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`categoryId`);

--
-- Constraints for table `categorytranslatesfr`
--
ALTER TABLE `categorytranslatesfr`
  ADD CONSTRAINT `categorytranslatesfr_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`categoryId`);

--
-- Constraints for table `orderheaders`
--
ALTER TABLE `orderheaders`
  ADD CONSTRAINT `orderheaders_ibfk_1` FOREIGN KEY (`customerId`) REFERENCES `customers` (`customerId`);

--
-- Constraints for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD CONSTRAINT `orderitems_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `orderheaders` (`orderId`),
  ADD CONSTRAINT `orderitems_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`categoryId`);

--
-- Constraints for table `producttranslatesen`
--
ALTER TABLE `producttranslatesen`
  ADD CONSTRAINT `producttranslatesen_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`);

--
-- Constraints for table `producttranslatesfr`
--
ALTER TABLE `producttranslatesfr`
  ADD CONSTRAINT `producttranslatesfr_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`);

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`userSCid`) REFERENCES `users` (`stripeCustId`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
